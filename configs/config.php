<?php

$config = array(
    'settings' => array(
        // View settings
        'view' => [
            'template_path' => APP_PATH . '/templates',
            'twig' => [
                'cache' => RUNTIME_PATH . '/twig',
                'debug' => true,
                'auto_reload' => true,
            ],
        ],
    ),

);
$config['debug'] = false; //отображение ошибок

return $config;