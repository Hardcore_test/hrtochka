<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 18.01.2018
 * Time: 12:54
 */

namespace dotCRM\Model;


class Task
{

    const PATH_CASH_FILE = RUNTIME_PATH . '/tmp.json';

    /** кол-во задач */
    const COUNT_TASK = 1000;

    /** время жизни кеша в секнудах */
    const TIME_CASH = 60 * 60;


    /**
     * Поиск по списку задач
     * @param int $start
     * @param int $count
     * @param string $title
     * @return array
     */
    public function getListTasks($start = 1, $count = 1000, $title = '')
    {
        $start--;
        $allTasks = $this->getAllTasks();
        $result = array(
            'tasks' => array(),
            'size' => 0,
        );
        /**
         * Проходим по всем задачам
         * в size добавляем все подходящие
         * в tasks начиная с начального значения и пока не наберем нужное количество
         */
        for ($i = 0; ($i < self::COUNT_TASK); $i++) {
            if ($this->existNeedle($allTasks[$i]['title'], $title)) {
                if ($result['size'] >= $start && count($result['tasks']) != $count) {
                    $result['tasks'][] = $allTasks[$i];
                }
                $result['size']++;
            }
        }
        return $result;

    }

    /**
     * Получает список всех задач, либо из кеша, либо генерирует
     * @return array
     */
    public function getAllTasks()
    {
        if ($this->checkActualCash()) {
            $tasks = $this->getTasksFromCash();
        } else {
            $tasks = $this->generateTasks();
        }
        return $tasks;
    }

    /**
     * Генерирует список задач и сохраняет в кеш
     * @return array
     */
    protected function generateTasks()
    {
        $tasks = array();
        for ($i = 1; $i <= self::COUNT_TASK; $i++) {
            $tasks[] = $this->generateTask($i);
        }
        $fp = fopen(self::PATH_CASH_FILE, 'w');
        fwrite($fp, json_encode($tasks));
        fclose($fp);
        clearstatcache(true, self::PATH_CASH_FILE);
        return $tasks;

    }

    /**
     * Генерерирует задачу по id
     * @param $id
     * @return array
     */
    public function generateTask($id)
    {
        $task = array(
            'id' => $id,
            'title' => "Задача $id",
            'date' => date('Y-m-d H:i:s', strtotime("$id hour")),
            'author' => "Автор $id",
            'status' => "Статус $id",
            'description' => "Описание $id",
        );

        return $task;
    }

    /**
     * возвращает список из кеша
     * @return array
     */
    protected function getTasksFromCash()
    {
        $json = file_get_contents(self::PATH_CASH_FILE);
        $tasks = json_decode($json, true);
        return $tasks;
    }

    /**
     * проверяет существует ли кеш и его актуальность
     * @return bool
     */
    protected function checkActualCash()
    {
        $result = false;
        if (file_exists(self::PATH_CASH_FILE)) {
            $fileTime = filemtime(self::PATH_CASH_FILE);
            $currentTime = time();
            if ($currentTime - $fileTime < self::TIME_CASH) {
                $result = true;
            }
        }
        return $result;
    }

    /**
     * существует ли подстрока в строке(считаем что пустая строка содержится в любой другой)
     * @param $haystack
     * @param $needle
     * @return bool
     */
    protected function existNeedle($haystack, $needle)
    {
        $haystack = trim(mb_strtolower($haystack));
        $needle = trim(mb_strtolower($needle));
        $result = (mb_strlen($needle) == 0) || (strpos($haystack, $needle) !== FALSE);
        return $result;
    }

}
