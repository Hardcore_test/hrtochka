<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.08.2016
 * Time: 15:54
 */

namespace dotCRM\Controller;


use dotCRM\Model\Task;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class TaskController
{

    const DIR = 'task/';


    public function index(Application $app, Request $request)
    {
        return $app['twig']->render(self::DIR . 'index.twig');
    }


    public function search(Application $app, Request $request)
    {
        $task = new Task();
        $start = $request->get('start', 0) + 1;
        $count = $request->get('length', 0);
        $search = $request->get('search', array('value' => ''));
        $title = $search['value'];
        $data = $task->getListTasks($start, $count, $title);
        echo json_encode(array(
            'draw' => $request->get('draw', 0),
            'recordsTotal' => 1000,
            'recordsFiltered' => $data['size'],
            'data' => $data['tasks'],
        ));
        die();
    }
}