<?php


namespace dotCRM\Controller\Provider;
use dotCRM\Controller\ApiController;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.08.2016
 * Time: 15:53
 */
class ApiProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        // creates a new controller based on the default route
        $controllers = $app['controllers_factory'];
        $app['controllers.bundle.api'] = function() use ($app) {
            return new ApiController();
        };

        $controllers->get('/task', "controllers.bundle.api:listTask")->bind("api_list_task");
        $controllers->get('/task/{id}', "controllers.bundle.api:getTaskById")->bind("api_task_id");

        return $controllers;
    }
}