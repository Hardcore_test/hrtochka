<?php


namespace dotCRM\Controller\Provider;
use dotCRM\Controller\TaskController;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;

/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.08.2016
 * Time: 15:53
 */
class TaskProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {

        // creates a new controller based on the default route
        $controllers = $app['controllers_factory'];
        $app['controllers.bundle.task'] = function() use ($app) {
            return new TaskController();
        };

        $controllers->get('', "controllers.bundle.task:index")->bind("task_index");
        $controllers->get('/search', "controllers.bundle.task:search")->bind("task_search");

        return $controllers;
    }
}