<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.08.2016
 * Time: 15:54
 */

namespace dotCRM\Controller;


use dotCRM\Model\Task;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class ApiController
{


    public function listTask(Application $app, Request $request)
    {
        $task = new Task();
        $data = $task->getListTasks();
        header('Content-Type: text/json');
        echo json_encode($data['tasks']);
        die();
    }

    public function getTaskById(Application $app, Request $request)
    {
        $taskId = $request->attributes->get('id');
        $task = new Task();
        $results = $task->generateTask($taskId);
        header('Content-Type: text/json');
        echo json_encode($results);
        die();
    }

}