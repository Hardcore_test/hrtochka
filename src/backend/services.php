<?php
/**
 * @var Silex\Application $app
 */
$app;


$app->register(new Silex\Provider\ServiceControllerServiceProvider());
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => TEMPLATES_PATH,
    'twig.options' => array(
        'cache' => RUNTIME_PATH . '/twig-cache',
        'debug' => $app['debug'],
        'auto_reload' => $app['debug'],
        'strict_variables' => $app['debug'],
    )
));

