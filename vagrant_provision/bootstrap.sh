if [ -f "/.bootstrap.done" ]; then
    service apache2 restart
    service elasticsearch restart
    exit
fi

DEBIAN_FRONTEND=noninteractive apt-get -y install software-properties-common python-software-properties
DEBIAN_FRONTEND=noninteractive add-apt-repository -y ppa:ondrej/php
DEBIAN_FRONTEND=noninteractive add-apt-repository -y ppa:ondrej/apache2
DEBIAN_FRONTEND=noninteractive add-apt-repository -y ppa:webupd8team/java
DEBIAN_FRONTEND=noninteractive apt-get -y update
DEBIAN_FRONTEND=noninteractive apt-get -y install php7.0
DEBIAN_FRONTEND=noninteractive apt-get -y install apache2
DEBIAN_FRONTEND=noninteractive apt-get -y install libapache2-mod-php7.0 php7.0-curl php7.0-json php7.0-dom\
 php7.0-gd php7.0-zip php7.0-dev unzip php7.0-mbstring



rm /etc/apache2/sites-enabled/000-default.conf
ln -s /vagrant/vagrant_provision/app.conf /etc/apache2/sites-enabled/001-app.conf
ln -s /etc/apache2/mods-available/rewrite.load /etc/apache2/mods-enabled/rewrite.load
service apache2 restart

php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"

php composer-setup.php --install-dir=/bin --filename=composer
php -r "unlink('composer-setup.php');"


service elasticsearch restart

touch /.bootstrap.done