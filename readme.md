Проект на [silex](https://silex.symfony.com/)   
Для сборки пакетов используется компосер  
Для фронта твиг  
Точка входа `public/index.php`  
Папка с контролерами `src/backend/Controller`  
Папка с провайдерами `src/backend/Provider`  
Папка с моделями `src/backend/Model`  
Роутеры `src/backend/routers`  
Севрисы `src/backend/services`  
Папка с шаблонами `src/templates`  
